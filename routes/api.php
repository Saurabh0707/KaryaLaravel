<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});
/**
Routes for Login. logout, refresh-token, register
 */
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LogoutController@logout')->name('logout');
Route::post('refresh', 'Auth\LogoutController@refresh')->name('refresh');
Route::post('register','Auth\RegisterController@register')->name('register');

Route::post('edit','User\UserController@edit')->name('edit');
Route::get('delete','User\UserController@delete')->name('delete');

Route::get('admin/clients','Admin\ClientsController@getClients')->name('clients');