<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    public function __construct()
    {

    }

    public function login(Request $request)
    {
        $rules = [
            'email'=>'required',
            'password'=>'required|min:6',
        ];
        $this->validate($request,$rules);
        $params=[
            'grant_type'=>'password',
            'client_id'=>request('client_id'),
            'client_secret'=>request('client_secret'),
            'username'=>request('email'),
            'password'=>request('password'),
            'scope'=>'*'
        ];
        $request->request->add($params);
        $makerequest=Request::create('oauth/token','POST');
        return Route::dispatch($makerequest);
    }
}
