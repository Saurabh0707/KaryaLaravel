<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    public function __construct()
    {

    }
    //function to retrieve the client credentials
    // public function getClient($client_id)
    // {
    //     //dynamically retrieving the client_id and secret
    //     $this->client=Client::find($client_id);
    // }
    /**
     * Register a user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $rules = [
            'user_name' =>'required',
            'first_name'=>'required',
            'last_name'=>'required',
            'phone'=>'required',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:6',
        ];
        $this->validate($request,$rules);
        $user = User::firstOrCreate([
            'name' => $request['user_name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);
        $userdetails = $user->userdetails()->firstOrCreate([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'phone' => $request['phone'],
            'activation_status' => 0,
        ]);
        $params=[
            'grant_type'=>'password',
            'client_id'=>request('client_id'),
            'client_secret'=>request('client_secret'),
            'username'=>request('email'),
            'password'=>request('password'),
            'scope'=>'*'
        ];
        $request->request->add($params);
        $makerequest=Request::create('oauth/token','POST');
        return Route::dispatch($makerequest);
    }
}