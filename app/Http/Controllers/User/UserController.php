<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except([]);
    }
    public function edit(Request $request){
        $rules = [
            'first_name'=>'required',
            'last_name'=>'required',
            'phone'=>'required',
            'password'=>'required|min:6',
        ];
        $this->validate($request,$rules);
        $x = Auth::user('api')->id;
        $user= User::find($x);
        $userDetails = $user->userdetails()->update([
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'phone' => $request['phone'],
                'activation_status' => $request['activation_status'],
                ]);
        if($userDetails)
        return response()->json(['message' => 'User Updated', 'code'=> 200],200);
        else return response()->json(['message' => 'User Not Updated', 'code'=> 200],200);
    }
    public function delete(){
            $id = Auth::user('api')->id;
            $user= User::find($id);
            Auth::user('api')->token()->revoke();
            $user->userdetails()->delete();
            $user->delete();
            return response()->json(['message' => 'User Soft Deleted', 'code'=> 200],200);
    }
}
